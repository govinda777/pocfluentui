﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pocFluentUi.Models;
using pocFluentUi.Validations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pocFluentUi.Controllers
{
    [Route("api/[controller]")]
    public class CustumerController : Controller
    {
        // GET: api/values
        [HttpGet]
        public IEnumerable<CustomerModel> Get()
        {
            var model = new CustomerModel[]
            {
                new CustomerModel() { BirthDate = (new DateTime(1990,01,01)).ToString(), Email = "teste@t.com", Id = Guid.NewGuid().ToString(), Name = "Teste T" },
                new CustomerModel() { BirthDate = new DateTime(1990,02,01).ToString(), Email = "teste2@t.com", Id = Guid.Empty.ToString(), Name = "Teste2 T" }
            };

            return model;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public CustomerModel Get(Guid id)
        {
            var model = new CustomerModel() { BirthDate = new DateTime(1990, 02, 01).ToString(), Email = "teste2@t.com", Id = Guid.Empty.ToString(), Name = "Teste2 T" };

            return model;
        }

        // POST api/values
        [HttpPost]
        public ActionResult Post([FromBody]CustomerModel model)
        {
            var validationResult = new RegisterNewCustomerValidation().Validate(model);

            if(!validationResult.IsValid)
            {
                return BadRequest(validationResult);
            }

            return Ok();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult Put(Guid id, [FromBody]CustomerModel model)
        {
            var validationResult = new UpdateNewCustomerValidation().Validate(model);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult);
            }

            return Ok();
        }
    }
}

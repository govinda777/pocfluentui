using pocFluentUi.Models;

namespace pocFluentUi.Validations
{
    public class RegisterNewCustomerValidation: CustomerValidation<CustomerModel>
    {
        public RegisterNewCustomerValidation()
        {
            ValidateName();
            ValidateBirthDate();
            ValidateEmail();
        }
    }
}
using pocFluentUi.Models;

namespace pocFluentUi.Validations
{
    public class UpdateNewCustomerValidation : CustomerValidation<CustomerModel>
    {
        public UpdateNewCustomerValidation()
        {
            ValidateId();
            ValidateName();
        }
    }
}
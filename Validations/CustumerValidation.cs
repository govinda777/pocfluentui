using FluentValidation;
using pocFluentUi.Models;
using System;

namespace pocFluentUi.Validations
{
public abstract class CustomerValidation<T> : AbstractValidator<T> where T : CustomerModel
    {
        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty()
                .WithMessage("Nome não informado");
        }

        protected void ValidateBirthDate()
        {
            RuleFor(c => c.BirthDate)
                .NotEmpty()
                .WithMessage("A pessoa deve ser maior que 18 anos");
        }

        protected void ValidateEmail()
        {
            RuleFor(c => c.Email)
                .NotEmpty()
                .WithMessage("E-mail não informado");
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty.ToString());
        }

}
}
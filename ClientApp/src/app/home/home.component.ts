import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  public custumers: Custumer[];
  private baseUrl: string;
  public modeNovo: Boolean;
  public custumerNovo: Custumer;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.baseUrl = baseUrl;
    this.getAll();
  }

  public getAll() {
    this.http.get<Custumer[]>(this.baseUrl + 'api/Custumer').subscribe(result => {
      this.custumers = result;
    },
    error => console.error(error));
  }

  public editar(item: Custumer) {
    item.modeEdit = true;
  }

  public novo() {
    this.modeNovo = true;
    this.custumerNovo = new Custumer();
  }

  public salvar(item: Custumer) {
    this.http.put<ValidatorResult[]>(this.baseUrl + 'api/Custumer/' + item.id, item)
    .subscribe(
    result => {
      this.getAll();
    },
    error => {

      if (error.error && !error.error.isValid) {
        item.validatorResults = error.error.errors;
      }
    });
  }

  public salvarNovo() {
    this.http.post<ValidatorResult[]>(this.baseUrl + 'api/Custumer', this.custumerNovo)
    .subscribe(
    result => {
      this.getAll();
    },
    error => {

      if (error.error && !error.error.isValid) {
        this.custumerNovo.validatorResults = error.error.errors;
      }
    });
  }
}

interface ValidatorResult {
  errorMessage: string;
  propertyName: string;
  errorCode: string;
}

interface ICustumer {
  id: string;
  name: string;
  email: string;
  birthDate: string;
  modeEdit: Boolean;
  validatorResults: ValidatorResult[];
}

class Custumer implements ICustumer {
  id: string = '';
  name: string = '';
  email: string = '';
  birthDate: string = '';
  modeEdit: Boolean;
  validatorResults: ValidatorResult[] = [];
  constructor() {}
}

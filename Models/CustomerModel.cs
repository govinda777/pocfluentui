using System;

namespace pocFluentUi.Models
{
    public class CustomerModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string BirthDate { get; set; }
    }
}